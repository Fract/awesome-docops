# DocOps

## Ressources

### Article 
- [Continuous Documentation - The best time is now](https://noti.st/kenigbolomeyastephen/gFreMs/continuous-documentation-the-best-time-is-now)
- [Continuous Documentation – Exemple de mise en place](http://www.salto-consulting.com/continuous-documentation-example/)
- [Continuous Documentation in a CI/CD World](https://thenewstack.io/continuous-documentation-in-a-ci-cd-world/)
- [Free and Open Source API Documentation Tools](https://pronovix.com/blog/free-and-open-source-api-documentation-tools)
- [Real Paths Toward Agile Documentation](https://thenewstack.io/real-paths-toward-agile-documentation/)
- [No Testing, No Documentation, No Problem?](https://thenewstack.io/no-testing-no-documentation-no-problem/)
- [Automation Is the Key for Agile API Documentation](https://thenewstack.io/automation-key-agile-api-documentation/)
- [WriteTheDocs](https://www.youtube.com/watch?v=uU0NwlMG6bs)
- [Building Docs Like Code: Continuous Integration for Documentation](https://www.youtube.com/watch?v=wEt_8twQctQ)
- [The DocOps Trend: Applying Agile and DevOps to Technical Documentation](https://www.liquidplanner.com/blog/the-docops-trend-applying-agile-and-devops-to-technical-documentation/)
- [DocOps: Intelligent Content for the Application Economy](https://contentmarketinginstitute.com/2015/04/intelligent-content-application-economy/)
- [DocOps: engineering great documentation](https://devrel.net/developer-experience/docops-engineering-great-documentation)
- [**DocOps Collection - DocHub**](https://doctoolhub.com/collection/docops/)
- [From DevOps to DocOps](https://www.avato-consulting.com/?p=28352&lang=en)



### Tools 
#### Technicals and users oriented documentation
- [OpenApi](https://swagger.io/resources/open-api/)
- [Sphinx](https://www.sphinx-doc.org/en/master/)
- [Docusaurus](https://docusaurus.io/)
- [ReadTheDocs](https://readthedocs.org/)
- [Github Pages](https://pages.github.com/)
- [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- [DropBox Paper](https://dropbox.com/paper) for internal use
- [Process Street](http://process.st/) for internal use
- [Tettra](https://tettra.co/) for internal use 

#### For API
- [Apiary](https://apiary.io/)
- [DapperBox](http://dapperdox.io/)
- [ReDoc](https://github.com/Rebilly/ReDoc)
- [Atlassian REST API Browser](https://marketplace.atlassian.com/apps/1211542/atlassian-rest-api-browser?hosting=server&tab=overview) for API use
- [RAML](https://raml.org/)
- [RAML2HTML](https://github.com/raml2html/raml2html) 
- [API blueprint](https://apiblueprint.org/)
- [Snowboard](https://github.com/subosito/snowboard)
- [Aglio](https://github.com/danielgtaylor/aglio)
- [I/O Docs](https://github.com/mashery/iodocs)
- [Slate](https://github.com/slatedocs/slate)
- [Whiteboard](https://github.com/mpociot/whiteboard)
- [APIDOC](https://apidocjs.com/)
- [DocBox](https://github.com/tmcw/docbox)
- [Carte](http://wiredcraft.github.io/carte/)



#### Code oriented documentation 

- [Doxygen](https://www.stack.nl/~dimitri/doxygen/) (C, C++, C♯, D, Fortran, IDL, Java, Objective-C, Perl, PHP, Python, Tcl, and VHDL)
- [GhostDoc](http://submain.com/products/ghostdoc.aspx) (C#, Visual Basic, C++/CLI, JavaScript)
- [Javadoc](http://www.oracle.com/technetwork/java/javase/documentation/index-jsp-135444.html) (Java only)
- [Docurium](https://www.ruby-toolbox.com/projects/docurium) (Ruby)
- [Dexy](http://www.dexy.it/)
- [Docco](http://ashkenas.com/docco/]




